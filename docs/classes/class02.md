## classes[1] = "The Molecules of Life"

---

## What is life?

* Controversial definitions  <!-- .element: class="fragment" data-fragment-index="1" -->
  * Physical entities <!-- .element: class="fragment" data-fragment-index="2" -->
  * Signaling & self-sustaining processes <!-- .element: class="fragment" data-fragment-index="2" -->
  * Associated with a specific environment <!-- .element: class="fragment" data-fragment-index="2" -->
* Viruses? <!-- .element: class="fragment" data-fragment-index="3" -->
* Synthetic life? <!-- .element: class="fragment" data-fragment-index="4" -->

---

## DNA

<img src="C02_assets/DNA.jpg" style="background:none; border:none; box-shadow:none;">

<p class="fragment">**D**eoxyribo**n**ucleic **a**cid</p>

---

## Where can we find DNA?

<img src="C02_assets/animal-cell.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

## Where can we find DNA?

<img src="C02_assets/plant-cell.jpg" style="background:none; border:none; box-shadow:none;">

---

## Where can we find DNA?

<img src="C02_assets/bacteria-cell.jpg" style="background:none; border:none; box-shadow:none;">

---

## Where can we find DNA?

<img src="C02_assets/virus.png" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C02_assets/Cell01.png" data-background-size="1024px">

---

<section data-background="C02_assets/Cell02.png" data-background-size="1024px">

---

## Nucleus

<img src="C02_assets/nucleus.png" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C02_assets/Cell03.png" data-background-size="1024px">

---

## Mitochondria

<img src="C02_assets/mitochondria.png" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C02_assets/Cell04.png" data-background-size="1024px">

---

## Ribosome

<img src="C02_assets/ribosome_01.jpg" style="background:none; border:none; box-shadow:none;">

---

<section data-background="C02_assets/Cell05.png" data-background-size="1024px">

---

## DNA structure

---

<section data-background="C02_assets/Chemical-structure-of-DNA.png" data-background-size="1024px">

---

<section data-background="C02_assets/4-nucleotides.png" data-background-size="1024px">

---

<section data-background="C02_assets/5-nucleotides.png" data-background-size="1024px">

---

## Nucleotides

![IUPAC](C02_assets/IUPAC.png)

IUPAC codes

|||

## Nuclotides

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=2 WIDTH="600" style="font-size:50%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC nucleotide code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Base</font></td>
</tr>
<tr>
<td>A</td>
<td>Adenine</td>
</tr>
<tr>
<td>C</td>
<td>Cytosine</td>
</tr>
<tr>
<td>G</td>
<td>Guanine</td>
</tr>
<tr>
<td>T (or U)</td>
<td>Thymine (or Uracil)</td>
</tr>
<tr>
<td>R</td>
<td>A or G</td>
</tr>
<tr>
<td>Y</td>
<td>C or T</td>
</tr>
<tr>
<td>S</td>
<td>G or C</td>
</tr>
<tr>
<td>W</td>
<td>A or T</td>
</tr>
<tr>
<td>K</td>
<td>G or T</td>
</tr>
<tr>
<td>M</td>
<td>A or C</td>
</tr>
<tr>
<td>B</td>
<td>C or G or T</td>
</tr>
<tr>
<td>D</td>
<td>A or G or T</td>
</tr>
<tr>
<td>H</td>
<td>A or C or T</td>
</tr>
<tr>
<td>V</td>
<td>A or C or G</td>
</tr>
<tr>
<td>N</td>
<td>any base</td>
</tr>
<tr>
<td>. or -</td>
<td>gap</td>
</tr>
</table>

---

<section data-background="C02_assets/Phospahte-backbone.png" data-background-size="1024px">

---

<section data-background="C02_assets/Watson-Crick-pair-rule.png" data-background-size="1024px">

---

<section data-background="C02_assets/Sense-DNA.png" data-background-size="1024px">

---

<section data-background="C02_assets/Sense-template.png" data-background-size="1024px">

---

## "Reading" DNA

AKA "Transcription": DNA - RNA <!-- .element: class="fragment" data-fragment-index="1" -->

![Transcription](C02_assets/transcription.png) <!-- .element: class="fragment" data-fragment-index="2" -->

This RNA is then moved towards a ribosome  <!-- .element: class="fragment" data-fragment-index="3" -->

---

## "Writing" DNA

AKA "Translation": RNA - Protein <!-- .element: class="fragment" data-fragment-index="1" -->

![Translation](C02_assets/ribosome_02.jpg) <!-- .element: class="fragment" data-fragment-index="2" -->

The RNA is translated into a protein <!-- .element: class="fragment" data-fragment-index="3" -->

---

## STOP!

<img src="C02_assets/stop-sign.png" style="background:none; border:none; box-shadow:none;">

---

## Central dogma of molecular biology

<img src="C02_assets/Central_dogma.png" style="background:none; border:none; box-shadow:none;">

|||

## Central dogma

*This states that once 'information' has passed into protein it cannot get out again. In more detail, the transfer of information from nucleic acid to nucleic acid, or from nucleic acid to protein may be possible, but transfer from protein to protein, or from protein to nucleic acid is impossible. Information means here the precise determination of sequence, either of bases in the nucleic acid or of amino acid residues in the protein.*

<p align="right">- Francis Crick, 1958</p>

---

### Translation Genetic code

<img src="C02_assets/codon_table.jpg" style="background:none; border:none; box-shadow:none;">

|||

### Standard genetic code

[Wikipedia table](https://en.wikipedia.org/wiki/DNA_codon_table)

|||

### AA IUPAC codes

<table BORDER CELLSPACING=0 CELLPADDING=2 COLS=3 WIDTH="700" style="font-size:45%">
<tr>
<td BGCOLOR="#B0C4DE"><font color="#000000">IUPAC amino acid code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Three letter code</font></td>
<td BGCOLOR="#B0C4DE"><font color="#000000">Amino acid</font></td>
</tr>
<tr>
<td>A</td>
<td>Ala</td>
<td>Alanine</td>
</tr>
<tr>
<td>C</td>
<td>Cys</td>
<td>Cysteine</td>
</tr>
<tr>
<td>D</td>
<td>Asp</td>
<td>Aspartic Acid</td>
</tr>
<tr>
<td>E</td>
<td>Glu</td>
<td>Glutamic Acid</td>
</tr>
<tr>
<td>F</td>
<td>Phe</td>
<td>Phenylalanine</td>
</tr>
<tr>
<td>G</td>
<td>Gly</td>
<td>Glycine</td>
</tr>
<tr>
<td>H</td>
<td>His</td>
<td>Histidine</td>
</tr>
<tr>
<td>I</td>
<td>Ile</td>
<td>Isoleucine</td>
</tr>
<tr>
<td>K</td>
<td>Lys</td>
<td>Lysine</td>
</tr>
<tr>
<td>L</td>
<td>Leu</td>
<td>Leucine</td>
</tr>
<tr>
<td>M</td>
<td>Met</td>
<td>Methionine</td>
</tr>
<tr>
<td>N</td>
<td>Asn</td>
<td>Asparagine</td>
</tr>
<tr>
<td>P</td>
<td>Pro</td>
<td>Proline</td>
</tr>
<tr>
<td>Q</td>
<td>Gln</td>
<td>Glutamine</td>
</tr>
<tr>
<td>R</td>
<td>Arg</td>
<td>Arginine</td>
</tr>
<tr>
<td>S</td>
<td>Ser</td>
<td>Serine</td>
</tr>
<tr>
<td>T</td>
<td>Thr</td>
<td>Threonine</td>
</tr>
<tr>
<td>V</td>
<td>Val</td>
<td>Valine</td>
</tr>
<tr>
<td>W</td>
<td>Trp</td>
<td>Tryptophan</td>
</tr>
<tr>
<td>Y</td>
<td>Tyr</td>
<td>Tyrosine</td>
</tr>
</table>

|||

## Start & Stop

<ul>
<li class="fragment">The 1<sup>st</sup> amino acid of any protein is always `Met`</li>
  <ul>
  <li class="fragment">`Met` is also called a "start codon"</li>
  <li class="fragment">`Met` is encoded by the nucleotide sequence `ATG`</li>
  </ul>
<li class="fragment">In order to finish a protein, a `STOP` codon is used</li>
  <ul>
  <li class="fragment">`TAA`</li>
  <li class="fragment">`TAG`</li>
  <li class="fragment">`TGA`</li>
  </ul>
</ul>

|||

## Frames

There are 6 possible reading frames:

```
Frm|ATGATCTGCTAA
-3 |AspLeuArgAsn
-2 |XXSTPValIle
-1 |XSerSerSer
+3 |  AspLeuLeuX
+2 | STPSerAlaXX
+1 |MetIleSerSTP
```

---

## In Vitro DNA replication

<ul>
<li class="fragment">PCR</li>
  <ul>
  <li class="fragment">Polymerase Chain Reaction</li>
  <li class="fragment">Amplifies a specific DNA region</li>
  </ul>
</ul>

<iframe class="fragment" width="560" height="315" src="https://www.youtube-nocookie.com/embed/JmveVAYKylk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

|||

## What can it be used for?

* Sequencing <!-- .element: class="fragment" data-fragment-index="1" -->
* Genotyping <!-- .element: class="fragment" data-fragment-index="2" -->
* Hybridization (Southern/Northern blot) <!-- .element: class="fragment" data-fragment-index="3" -->
* Measuring gene expression (Quantitative PCR) <!-- .element: class="fragment" data-fragment-index="4" -->

---

## Sequencing DNA

Sanger Method

<img src="C02_assets/sanger.jpg" style="background:none; border:none; box-shadow:none;">

|||

## Sequencing DNA

Illumina method

<img src="C02_assets/Illumina.png" style="background:none; border:none; box-shadow:none;">

|||

## Sequencing DNA

Oxford Nanopore

<img src="C02_assets/nanopore.jpg" style="background:none; border:none; box-shadow:none;">

---

## Summary

* Where is DNA? <!-- .element: class="fragment" data-fragment-index="1" -->
* DNA structure <!-- .element: class="fragment" data-fragment-index="2" -->
* DNA representation <!-- .element: class="fragment" data-fragment-index="3" -->
* Transcription & translation <!-- .element: class="fragment" data-fragment-index="4" -->
* Central dogma of molecular biology <!-- .element: class="fragment" data-fragment-index="5" -->
* Standard genetic code <!-- .element: class="fragment" data-fragment-index="6" -->
* DNA Sequencing <!-- .element: class="fragment" data-fragment-index="7" -->

---

## Homework

* Write up a 21 base pair sequence that contains 4 ambiguities
* Re-write 3 of the possible unambiguous sequences
* Pick one of these sequences and write:
  * Its *reverse*
  * Its *complement*
  * Its *reverse-complement*
  * The amino-acid chain it produces (use the 1 letter encoding)

|||

## Homework

* Due date: Next class
* Delivery: email
* Format:
  * `Your_name_Student_Num.txt`

```
>Sequence identifier
ATGCTCGATCG...
>Another sequence identifier
GCTATCTGACT...
```

