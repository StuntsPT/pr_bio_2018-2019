### classes[7] = "A gentle intro to programming"

#### Perspetivas em bioinformática 2018-2019

![Logo EST](C07_assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

---

### Python

<ul>
<li class="fragment">"General purpose" language</li>
  <ul>
  <li class="fragment">Interpreted language</li>
  <li class="fragment">Focused on coead readability</li>
  <li class="fragment">High level language</li>
  <li class="fragment">Object-oriented</li>
  <li class="fragment">Imperative</li>
  <li class="fragment">Functional</li>
  </ul>
</ul>

<img class="fragment" src="C08_assets/python_logo.png" style="background:none; border:none; box-shadow:none;">

---

### Installing python

<ul>
<li class="fragment">Python 3 is installe dby default on most GNU/Linux distributions</li>
<li class="fragment">How do we write python code?</li>
<li class="fragment">How do we run python code?</li>
  <ul>
  <li class="fragment">We need a code editor</li>
  <li class="fragment">*Vim* and *Gedit* will do fine</li>
  <li class="fragment">We can do better</li>
  </ul>
</ul>

|||

### Installing python

```bash
sudo apt update
sudo apt install idle3
```

---

### Now live!

<img src="C07_assets/now-live-icon.png" style="background:none; border:none; box-shadow:none;">

---

### Usefull resources

* [learnpython.org](https://www.learnpython.org/en/)
* [The python tutorial](https://docs.python.org/3/tutorial/index.html)
* [The python guru](https://thepythonguru.com/)
* [devmedia](https://www.devmedia.com.br/download-do-python-e-os-primeiros-passos/37003) (Em Português do Brasil)
* [Mak Summerfield](http://cs.stmarys.ca/~porter/csc/227/ProgrammingInPython3.pdf) (PDF Book)
* [Programming in Python 3](https://www.amazon.com/Programming-Python-Complete-Introduction-Language/dp/0321680561) (Paper book)
